import pandas as pd
from concurrent.futures import ProcessPoolExecutor  # , as_completed

import gc

from psutil import Process
import os
pid = os.getpid()


def df_parallel_apply(df, func, axis=0, *args, chunksize=1000, max_workers=8, **kwargs):
    """
    Parallelized version for pandas.DataFrame.apply.

    Parameters
    ----------
    df : [type]
        [description]
    func : [type]
        [description]
    axis : int, optional
        [description], by default 0
    chunksize : int, optional
        [description], by default 1000
    max_workers : int, optional
        [description], by default 8

    Returns
    -------
    [type]
        [description]
    """
    print("parallel apply")
    axis = 1 if axis in [ 1, 'columns' ] else 0
    if axis == 1:
        chunks = [ df.iloc[start:start + chunksize] for start in range(0, len(df), chunksize) ]
    else:
        chunks = [ df.iloc[:, start:start + chunksize] for start in range(0, len(df.columns), chunksize) ]
    print("chunks generated")
    with ProcessPoolExecutor(max_workers=max_workers) as executor:
        jobs = [
            executor.submit(
                pd.DataFrame.apply,
                *(( chunk, ) + args),
                **{
                    'func': func,
                    'axis': axis,
                    **kwargs
                }
            )
            for chunk in chunks
        ]
        print("workers created")
        # almost_results = as_completed(jobs)
        results = []
        for res in jobs:
            print(Process(pid).memory_info())
            result = res.result(timeout=None)
            del res
            gc.collect()
            print(result.index.min(), result.index.max())
            results.append(result)
        # results = [ result.result() for result in almost_results ]
        print("results computed")

    return pd.concat(results)
